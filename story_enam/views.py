from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import Message
from .forms import Message_Form

# Create your views here.
def index(request):
	response['daftar'] = Message_Form
	return render(request, 'homepage.html', response)

#def profile(request):
#	return render(request, 'profile.html', response)

response = {}
def add_status(request):
	response['daftar'] = Message_Form
	form = Message_Form(request.POST)
	html = 'homepage.html'
	if request.method =='POST':
		response['homepage_input'] = request.POST['homepage_input']
		status = Message(homepage_input=response['homepage_input'])
		status.save()
		response['daftarStatus'] = Message.objects.all()
		return render(request, html, response)
	else:
		return render(request, html, response)

# def result_table(request):
# 	response = {'daftarStatus': Message_Form, 'status': Message.objects.all()}
# 	html = 'homepage.html'
# 	return render(request, html, response)

# def delete_all(request):
# 	Message.objects.all().delete()
# 	return HttpResponseRedirect('result_table')

