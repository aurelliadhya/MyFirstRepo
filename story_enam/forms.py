from django import forms

class Message_Form(forms.Form):
	error_messages = {
        'required': 'Please fill this field',
    }
	attrs = {
		'class': 'form-control'
    }

	homepage_input = forms.CharField(max_length=300, widget=forms.TextInput(attrs=attrs), required=True)
