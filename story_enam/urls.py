"""lab4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views
#from .views import profile
from .views import index
from .views import add_status


app_name = 'story_enam'
urlpatterns = [
    path('', index, name='index'),
    #path('profile', profile, name='profile'),
    path('add_status', views.add_status, name = 'add_status'),
    # path('delete_all', views.delete_all, name='delete_all'),
    # path('result_table', views.result_table, name = 'result_table'),
]
