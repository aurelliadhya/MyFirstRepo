"""lab4 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from . import views
from .views import result_table

app_name = 'story_empat'
urlpatterns = [
    path('', views.Home, name='Home'),
    path('Home', views.Home, name='Home'),
    path('PersonalInterests', views.PersonalInterests, name='PersonalInterests'),
    path('Feedback', views.Feedback, name='Feedback'),
    path('registeraccount', views.registeraccount, name='registeraccount'),
    path('add_activity', views.add_activity, name = 'add_activity'),
    path('form_page', views.form_page, name='form_page'),
    path('result_table', views.result_table, name='result_table'),
    path('delete_all', views.delete_all, name='delete_all'),
]
