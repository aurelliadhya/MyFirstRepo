from django.db import models

# Create your models here.
class JadwalPribadi(models.Model):
	nama_kegiatan = models.CharField(max_length=30)
	nama_tempat = models.CharField(max_length=30)
	tanggal_kegiatan = models.DateField()
	waktu_kegiatan = models.TimeField()
	kategori_kegiatan = models.CharField(max_length=30)
