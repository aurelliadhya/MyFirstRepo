from django import forms

class Project_Form(forms.Form):
	error_messages = {
        'required': 'Please fill this field',
    }
	attrs = {
		'class': 'form-control'
    }

	nama_kegiatan = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrs), required=True)
	nama_tempat = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrs), required=True)
	tanggal_kegiatan = forms.DateField(widget = forms.DateInput(attrs={'type':'date', 'class': 'form-control'}))
	waktu_kegiatan = forms.TimeField(widget = forms.TimeInput(attrs={'type':'time', 'class': 'form-control'}))
	kategori_kegiatan = forms.CharField(max_length=30, widget=forms.TextInput(attrs=attrs), required=True)
