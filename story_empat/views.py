from django.http import HttpResponseRedirect
from django.shortcuts import render
from .models import JadwalPribadi
from .forms import Project_Form

response = {}
# Create your views here.
def Home(request):
    response = {}
    return render(request, 'Home.html', response)

def PersonalInterests(request):
    response = {}
    return render(request, 'PersonalInterests.html', response)

def Feedback(request):
    response = {}
    return render(request, 'Feedback.html', response)

def registeraccount(request):
    response = {}
    return render(request, 'registeraccount.html', response)

def form_page(request):
	response = {}
	response['message_form'] = Project_Form
	return render(request, 'form_page.html', response)

def add_activity(request):
	response = {}
	form = Project_Form(request.POST or None)
	html = 'form_result.html'
	if request.method=='POST' and 'add' in request.POST:
		response['nama_kegiatan'] = request.POST['nama_kegiatan']
		response['nama_tempat'] = request.POST['nama_tempat']
		response['tanggal_kegiatan'] = request.POST['tanggal_kegiatan']
		response['waktu_kegiatan'] = request.POST['waktu_kegiatan']
		response['kategori_kegiatan'] = request.POST['kategori_kegiatan']
		schedule = JadwalPribadi(nama_kegiatan=response['nama_kegiatan'], nama_tempat=response['nama_tempat'], tanggal_kegiatan=response['tanggal_kegiatan'], waktu_kegiatan=response['waktu_kegiatan'], kategori_kegiatan=response['kategori_kegiatan'])
		schedule.save()
		return render(request, html, response)
	else:
		return HttpResponseRedirect('story_empat: form_result')

def result_table(request):
	response['result'] = JadwalPribadi.objects.all()
	html = 'table.html'
	return render(request, html, response) 

def delete_all(request):
	JadwalPribadi.objects.all().delete()
	return HttpResponseRedirect('result_table')