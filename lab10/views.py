from django.shortcuts import render
from django.http import JsonResponse
from .forms import SubscribeForm
from .models import Subscriber
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
response = {}

def subscribe(request):
	response = {'forms' : SubscribeForm}
	return render(request, 'subscribe.html', response)

@csrf_exempt
def validate(request):
    email = request.POST.get("email")
    exist = Subscriber.objects.filter(email=email)
    if exist:
        data = {
            'not_valid': True
        }
    else:
        data = {
            'not_valid': False
        }
    return JsonResponse(data)


def add_subscribe(request):
	if request.method == 'POST':
		form = SubscribeForm(request.POST)
		print(form)
		if (form.is_valid()):
			# response['name'] = request.POST['name']
			# response['email'] = request.POST['email']
			# response['password'] = request.POST['password']
			# subscriber = Subscriber(name=response['name'], email=response['email'], password=response['password'])
			# subscriber.save()
			subscriber = form.cleaned_data
			subs = Subscriber(name=subscriber['name'], email=subscriber['email'], password=subscriber['password'])
			subs.save()
		return JsonResponse({'name': subscriber['name'], 'email': subscriber['email'], 'password': subscriber['password']})
	