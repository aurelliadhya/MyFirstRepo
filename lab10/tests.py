from django.test import TestCase, Client
from django.urls import resolve
from .views import subscribe

# Create your tests here.
# class Story10Test(TestCase):
#     def test_url_is_exist(self):
#         response = Client().get('/')
#         self.assertEqual(response.status_code, 200)
#     def test_using_subscribe_template(self):
#         response = Client().get('/')
#         self.assertTemplateUsed(response, 'subscribe.html')
#     def test_title(self):
#         response = Client().get('/')
#         html_response = response.content.decode('utf8')
#         self.assertIn('Go Subscribe!', html_response)