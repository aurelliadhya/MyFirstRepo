from django.test import TestCase, Client
from django.urls import resolve
from .views import tambah, kurang

# Create your tests here.
class Lab9Test(TestCase):
	def test_login_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code,200)

	def test_home_url_is_exist(self):
		response = Client().get('/home')
		self.assertEqual(response.status_code,200)

	def test_name_title(self):
		response = Client().get('')
		homepage_title = response.content.decode('utf8')
		self.assertIn('Login', homepage_title)

	def test_website_using_html(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'login.html')

	def test_get_correct_output_html(self):
		response = Client().get('/home')
		html_response = response.content.decode('utf8')
		self.assertIn('Book List', html_response)

	def test_lab_11_using_tambah(self):
		found = resolve('/tambah')
		self.assertEqual(found.func, tambah)

	def test_lab_11_using_kurang(self):
		found = resolve('/kurang')
		self.assertEqual(found.func, kurang)