$(document).ready(function() {
	$.ajax({
		url: "data",
		datatype: 'json',
		success: function(data){
			var result ='<tr>';
			for(var i = 0; i < data.items.length; i++) {
				result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'><center>" + data.items[i].volumeInfo.authors + "</center></td>" + 
				"<td class='align-middle'><center>" + data.items[i].volumeInfo.publisher +"</center></td>" + 
				"<td class='align-middle'><center>" + data.items[i].volumeInfo.publishedDate +"</center></td>" + 
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});

var counter = 0;
function favorite(clicked_id){
	var button = document.getElementById(clicked_id);
	if(button.classList.contains("checked")){
		button.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		$.ajax({
			url: "kurang",
			dataType: 'json',
			success: function(result){
				var counter = JSON.parse(result);
				console.log(result)
				document.getElementById("counter").innerHTML = counter;
				$('#counter').html(counter);
			}
		});
	}
	else {
		button.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/148/148841.svg';
		$.ajax({
			url: "tambah",
			dataType: 'json',
			success: function(result){
				var counter = JSON.parse(result);
				console.log(result)
				document.getElementById("counter").innerHTML = counter;
				$('#counter').html(counter);
			}
		});
	}
}

// function onSignIn(googleUser) {
// 	var profile = googleUser.getBasicProfile();
// 	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
// 	console.log('Name: ' + profile.getName());
// 	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
// }

// function signOut() {
//     var auth2 = gapi.auth2.getAuthInstance();
//     auth2.signOut().then(function () {
//       console.log('User signed out.');
//     });
// }


