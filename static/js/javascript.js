$(document).ready(function() {
    var name = "";
    var password = "";
    var emailIsValid = false;
    $("#name").change(function() {
        name = $(this).val();
    });
    $("#password").keyup(function() {
        password = $(this).val();
        console.log(emailIsValid);
        if(name.length > 0 && password.length > 7 && emailIsValid) {
            $("#button-button-3").prop("disabled", false);
        }
    })
    $("#email").change(function() {
        var email = $(this).val();
        $.ajax({
            url: "/validate/",
            data: {
                'email': email,
            },
            type: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data['not_valid']);
                if(data['not_valid'] == true) {
                    alert("This email is already exist, please use a new one.");
                } else {
                    emailIsValid = true;
                    if(name.length > 0 && password.length > 7) {
                        $("#button-button-3").prop("disabled", false);
                    }
                }
                
            }
        });
    });
    $("#button-button-3").click(function() {
        event.preventDefault();
        $.ajax({
            url:"/add_subscribe/",
            type: "POST",
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            data: $("form").serialize(),
            dataType: 'json',
            success: function(data){
                alert("SUCCESS!");
                document.getElementById('name').value = '';

                document.getElementById('email').value = '';

                document.getElementById('password').value = '';

                $("#button-button-3").prop("disabled", true);  

            },
            error: function(err){
                alert('aweqwe');
            }

        })
    });
});
