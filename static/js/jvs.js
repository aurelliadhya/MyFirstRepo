var color = ["#004e64", "#244a2c", "#4b423d"];
var accordion_color = ["#004e64", "#244a2c", "#4b423d"];

$(document).ready(function(){
	var acc = document.getElementsByClassName("accordion");
	var i;
	for (i = 0; i < acc.length; i++) {
		acc[i].addEventListener("click", function() {
			this.classList.toggle("active");
			var panel = this.nextElementSibling;
			if (panel.style.display === "block") {
				panel.style.display = "none";
			} else {
				panel.style.display = "block";
			}
		});
	}
	var i = 0;

	$("#button").click(function(){
		i = i < color.length ? ++i : 0;
		document.querySelector("body").style.background = color[i]
	})
})

var myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}