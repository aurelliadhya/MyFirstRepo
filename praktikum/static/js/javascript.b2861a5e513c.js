$(document).ready(function() {
    var name = "";
    var password = "";
    var emailIsValid = false;
    $("#id_name").change(function() {
        name = $(this).val();
    });
    $("#id_password").keyup(function() {
        password = $(this).val();
        console.log(emailIsValid);
        if(name.length > 0 && password.length > 7 && emailIsValid) {
            $("#button-button-3").prop("disabled", false);
        }
    })
    $("#id_email").change(function() {
        var email = $(this).val();
        $.ajax({
            url: "/validate/",
            data: {
                'email': email,
            },
            method: "POST",
            dataType: 'json',
            success: function(data) {
                console.log(data['not_valid']);
                if(data['not_valid'] == true) {
                    alert("This email is already exist, please use a new one.");
                } else {
                    emailIsValid = true;
                    if(name.length > 0 && password.length > 7) {
                        $("#button-button-3").prop("disabled", false);
                    }
                }
                
            }
        });
    });
    $("#button-button-3").click(function() {
        event.preventDefault();
        $.ajax({
            headers: {"X-CSRFToken": $("input[name=csrfmiddlewaretoken]").val()},
            url:"/subscribe/",
            data: $("form").serialize(),
            method: 'POST',
            dataType: 'json',
            success: function(data){
                alert("SUCCESS!");
                document.getElementById('id_name').value = '';

                document.getElementById('id_email').value = '';

                document.getElementById('id_password').value = '';

                $("#button-button-3").prop("disabled", true);  

            }
        })
    });
});
