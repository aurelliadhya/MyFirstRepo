$(document).ready(function() {
	$.ajax({
		url: "data",
		datatype: 'json',
		success: function(data){
			var result ='<tr>';
			for(var i = 0; i < data.items.length; i++) {
				result += "<th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
				"<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
				"<td class='align-middle'><center>" + data.items[i].volumeInfo.authors + "</center></td>" + 
				"<td class='align-middle'><center>" + data.items[i].volumeInfo.publisher +"</center></td>" + 
				"<td class='align-middle'><center>" + data.items[i].volumeInfo.publishedDate +"</center></td>" + 
				"<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
				"<td class='align-middle' style='text-align:center'>" + "<img id='bintang" + i + "' onclick='favorite(this.id)' width='28px' src='https://image.flaticon.com/icons/svg/149/149222.svg'>" + "</td></tr>";
			}
			$('tbody').append(result);
		}
	})
});

var counter = 0;
function favorite(clicked_id){
	var button = document.getElementById(clicked_id);
	if(button.classList.contains("checked")){
		button.classList.remove("checked");
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/149/149222.svg';
		counter--;
		document.getElementById("counter").innerHTML = counter;
	}
	else{
		button.classList.add('checked');
		document.getElementById(clicked_id).src = 'https://image.flaticon.com/icons/svg/148/148841.svg';
		counter++;
		document.getElementById("counter").innerHTML = counter;
	}
}

function onSignIn(googleUser) {
	var profile = googleUser.getBasicProfile();
	console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
	console.log('Name: ' + profile.getName());
	console.log('Image URL: ' + profile.getImageUrl());
	console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
}

function signOut() {
    var auth2 = gapi.auth2.getAuthInstance();
    auth2.signOut().then(function () {
      console.log('User signed out.');
    });
}

$('#google-button').on('click', function() {
  // Initialize with your OAuth.io app public key
  OAuth.initialize('815068721581-j18ic1s6limjvgo8q116mvae6n6r7lep.apps.googleusercontent.com');
  // Use popup for oauth
  OAuth.popup('google').then(google => {
    console.log('google:',google);
    // Retrieves user data from oauth provider
    // Prompts 'welcome' message with User's email on successful login
    // #me() is a convenient method to retrieve user data without requiring you
    // to know which OAuth provider url to call
    google.me().then(data => {
      console.log('me data:', data);
      alert('Google says your email is:' + data.email + ".\nView browser 'Console Log' for more details");
    });
    // Retrieves user data from OAuth provider by using #get() and
    // OAuth provider url
    google.get('/plus/v1/people/me').then(data => {
      console.log('self data:', data);
    })
  });
})
