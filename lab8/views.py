from django.shortcuts import render

# Create your views here.
def landpage(request):
	response = {}
	return render(request, 'landpage.html', response)